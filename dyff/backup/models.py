# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

# mypy: disable-error-code="import-untyped"

from __future__ import annotations

import datetime
import enum
from functools import singledispatch
from typing import Any, List, Mapping, Optional, Union

import click
import kubernetes as k8s
import ruamel.yaml

from dyff.client import Client
from dyff.schema.platform import (
    SYSTEM_ATTRIBUTES,
    Dataset,
    DyffSchemaBaseModel,
    Entities,
    Evaluation,
    InferenceService,
    InferenceSession,
    Model,
    ModelStorageMedium,
    Report,
    Resources,
)

from .config import config

# FIXME: All of these k8s-related functions are copy-pasted from
# dyff-orchestrator. Move to shared location.


def now() -> datetime.datetime:
    return datetime.datetime.now(datetime.timezone.utc)


def dt_to_str(dt: datetime.datetime) -> str:
    return dt.isoformat()


def now_str() -> str:
    return dt_to_str(now())


def parse(t: datetime.datetime | str | None) -> datetime.datetime | None:
    if t is None:
        return None
    elif isinstance(t, str):
        return datetime.datetime.fromisoformat(t)
    elif isinstance(t, datetime.datetime):
        return t
    else:
        raise TypeError(f"t: {type(t).__name__}")


YAMLScalar = Union[str, int, float, bool, None]
YAMLType = Union[YAMLScalar, List["YAMLType"], Mapping[str, "YAMLType"]]
YAMLList = List[YAMLType]
YAMLObject = Mapping[str, YAMLType]


@singledispatch
def to_yaml(spec: Any) -> YAMLType:
    if spec is None:
        return None
    else:
        raise TypeError(f"No conversion defined for {type(spec)}")


@to_yaml.register
def _(spec: datetime.datetime) -> str:
    return dt_to_str(spec)


@to_yaml.register
def _(spec: enum.Enum) -> YAMLType:
    return spec.name


@to_yaml.register(str)
def _(spec) -> YAMLScalar:
    # If type uses the Subclass(str, Enum) pattern, singledispatch will detect
    # it as a str.
    if isinstance(spec, enum.Enum):
        return spec.name
    else:
        return spec


@to_yaml.register(int)
@to_yaml.register(float)
@to_yaml.register(bool)
def _(spec) -> YAMLScalar:
    return spec


@to_yaml.register
def _(spec: dict) -> YAMLObject:
    return {k: to_yaml(v) for k, v in spec.items()}


@to_yaml.register
def _(spec: list) -> YAMLList:
    return [to_yaml(x) for x in spec]


@to_yaml.register(Dataset)
@to_yaml.register(Evaluation)
@to_yaml.register(InferenceService)
@to_yaml.register(InferenceSession)
@to_yaml.register(Model)
@to_yaml.register(Report)
def _(spec: DyffSchemaBaseModel) -> YAMLObject:
    d = spec.dict()
    for k in SYSTEM_ATTRIBUTES:
        d.pop(k, None)
    return to_yaml(d)  # type: ignore


_DYFF_GROUP = "dyff.io"
_DYFF_VERSION = "v1alpha1"
_DYFF_API_VERSION = f"{_DYFF_GROUP}/{_DYFF_VERSION}"

_short_names = {
    Entities.Audit: "audit",
    Entities.Dataset: "data",
    Entities.Evaluation: "eval",
    Entities.InferenceService: "infer",
    Entities.InferenceSession: "session",
    Entities.Model: "model",
    Entities.Report: "report",
}


_component_names = {
    Entities.Audit: {"fetch": "fetch", "proxy": "proxy", "run": "run"},
    Entities.Dataset: {
        "ingest": "ingest",
    },
    Entities.Evaluation: {
        "client": "client",
        "inference": "infer",
        "verification": "verify",
    },
    Entities.InferenceService: {
        "build": "build",
    },
    Entities.InferenceSession: {
        "inference": "infer",
    },
    Entities.Model: {
        "fetch": "fetch",
    },
    Entities.Report: {
        "run": "run",
    },
}


def _object_name(kind: Entities | str, id: str, component=None) -> str:
    kind = Entities(kind)
    short_name = _short_names[kind]
    if component:
        component_name = _component_names[kind][component]
        return f"{short_name}-{id}-{component_name}"
    else:
        return f"{short_name}-{id}"


def _maybe_namespaced(manifest: dict, *, namespace: Optional[str] = None) -> dict:
    if namespace:
        manifest = manifest.copy()
        manifest["metadata"]["namespace"] = namespace
    return manifest


def _model_manifest(model: Model, *, namespace: Optional[str] = None) -> Optional[dict]:
    """Translate a Dyff API ``Model`` to a k8s ``Model``."""
    spec: dict[str, Any] = {
        "id": model.id,
        "account": model.account,
        # Note: We're assuming this sub-schema matches k8s exactly
        "source": model.source.dict(exclude_none=True),
    }

    if model.storage.medium == ModelStorageMedium.PersistentVolume:
        spec["storage"] = {
            "kind": "PersistentVolume",
            "persistentVolume": {"storageClassName": "dyff-model"},
        }
    else:
        raise NotImplementedError(f"model.storage.medium: {model.storage.medium}")

    spec["storage"]["quantity"] = model.resources.storage

    manifest = {
        "apiVersion": _DYFF_API_VERSION,
        "kind": "Model",
        "metadata": {
            "name": _model_name(spec["id"]),
            "labels": {
                f"{_DYFF_GROUP}/workflow": "model",
                f"{_DYFF_GROUP}/component": "model",
                f"{_DYFF_GROUP}/account": spec["account"],
                f"{_DYFF_GROUP}/id": spec["id"],
            },
        },
        "spec": to_yaml(spec),
    }
    return _maybe_namespaced(manifest, namespace=namespace)


def _model_name(id: str, component=None) -> str:
    """Returns the k8s name of a Model-related component.

    Args:
      component: Could be ``"fetch"``, or ``None``
    """
    return _object_name(Entities.Model, id, component)


def _create_resource(kind: Entities | str, body, *, namespace: str) -> Any:
    kind = Entities(kind)
    return k8s.client.CustomObjectsApi().create_namespaced_custom_object(
        group=_DYFF_GROUP,
        version=_DYFF_VERSION,
        namespace=namespace,
        plural=Resources.for_kind(kind).value,
        body=body,
    )


def _rox_manifest(model: Model, *, namespace: Optional[str] = None) -> dict:
    # apiVersion: v1
    # kind: PersistentVolumeClaim
    # metadata:
    #   name: model-4b3f71db91de4add8ea096e9fbd1c580-rox
    # spec:
    #   dataSource:
    #     kind: PersistentVolumeClaim
    #     name: model-4b3f71db91de4add8ea096e9fbd1c580-storage
    #   accessModes:
    #     - ReadOnlyMany
    #   storageClassName: dyff-model
    #   resources:
    #     requests:
    #       storage: "20Gi"
    manifest = {
        "apiVersion": "v1",
        "kind": "PersistentVolumeClaim",
        "metadata": {
            "name": f"{_model_name(model.id)}-rox",
        },
        "spec": {
            "dataSource": {
                "kind": "PersistentVolumeClaim",
                "name": f"{_model_name(model.id)}-storage",
            },
            "accessModes": [
                "ReadOnlyMany",
            ],
            "storageClassName": "dyff-model",
            "resources": {"requests": {"storage": model.resources.storage}},
        },
    }
    return _maybe_namespaced(manifest, namespace=namespace)


@click.group()
def models():
    pass


@models.command()
@click.option(
    "namespace",
    "--namespace",
    metavar="NAMESPACE",
    type=str,
    default=None,
    help="Kubernetes namespace for resources. Default: empty (=> use current)",
)
@click.option(
    "models_out",
    "--models-out",
    metavar="MODELS_OUT",
    type=str,
    required=True,
    default=None,
    help="Output file for models.dyff.io manifests",
)
@click.option(
    "rox_out",
    "--rox-out",
    metavar="ROX_OUT",
    type=str,
    required=True,
    default=None,
    help="Output file for ReadOnlyMany PVC manifests",
)
def create_manifests(models_out: str, rox_out: str, namespace: Optional[str] = None):
    yaml = ruamel.yaml.YAML()
    dyffapi = Client(
        api_key=config.backup.api_token.get_secret_value(),
        endpoint=config.backup.api_endpoint,
    )
    models = dyffapi.models.query(status="Ready")
    manifests = [_model_manifest(model, namespace=namespace) for model in models]
    rox_manifests = [_rox_manifest(model, namespace=namespace) for model in models]

    with open(models_out, "w") as models_stream:
        with open(rox_out, "w") as rox_stream:
            yaml.dump_all(manifests, models_stream)
            yaml.dump_all(rox_manifests, rox_stream)
