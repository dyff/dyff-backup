# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

# mypy: disable-error-code="import-untyped"

from __future__ import annotations

import base64
import json
import multiprocessing as mp
import pathlib
import queue
import threading
import time
import uuid
from typing import Iterable, NamedTuple, Optional

import click
import confluent_kafka as kafka
import pydantic

from dyff import storage
from dyff.schema.platform import (
    DyffEntityType,
    EntityStatus,
    EntityStatusReason,
    is_status_terminal,
)

from .config import config


class _InputItem(NamedTuple):
    message_id: str
    object_path: str


class _OutputItem(NamedTuple):
    worker_id: int
    message_id: str
    success: bool
    message: Optional[str] = None


class _Worker(mp.Process):
    def __init__(
        self,
        worker_id: int,
        in_queue: mp.Queue[list[_InputItem] | None],
        out_queue: mp.Queue[_OutputItem | None],
        *,
        dryrun: bool = False,
    ):
        super().__init__()

        self.id = worker_id
        self._in_queue = in_queue
        self._out_queue = out_queue
        self._pending: set[str] = set()
        self._dryrun = dryrun

    def _on_produce_acknowledged(self, error, msg):
        try:
            message_id = msg.key().decode("utf-8")

            if error:
                self._out_queue.put(
                    _OutputItem(
                        worker_id=self.id,
                        message_id=message_id,
                        success=False,
                        message=str(error),
                    )
                )
            else:
                try:
                    self._pending.remove(message_id)
                    self._out_queue.put(
                        _OutputItem(
                            worker_id=self.id, message_id=message_id, success=True
                        )
                    )
                except Exception as ex:
                    self._out_queue.put(
                        _OutputItem(
                            worker_id=self.id,
                            message_id=message_id,
                            success=False,
                            message=" ".join(ex.args),
                        )
                    )
        except:
            self._out_queue.put(
                _OutputItem(
                    worker_id=self.id,
                    message_id="?????",
                    success=False,
                    message="failed to decode msg.key()",
                )
            )

    def run(self) -> None:
        producer_config = config.kafka.config.get_producer_config()
        if producer_config["client.id"] is None:
            producer_config["client.id"] = f"dyff.backup.restore.{self.id}"
        producer = kafka.Producer(producer_config)

        while True:
            try:
                input_chunk = self._in_queue.get_nowait()

                if input_chunk is None:
                    # sentinel
                    break

                for input_item in input_chunk:
                    try:
                        object_bytes = storage.get_object(input_item.object_path).read()
                        obj = json.loads(object_bytes.decode("utf-8"))
                        msg = obj["kafka.Message"]
                        key = base64.b64decode(msg["key"])
                        value = base64.b64decode(msg["value"])

                        # Verify that the payload is well-formed
                        entity: DyffEntityType = pydantic.parse_raw_as(
                            DyffEntityType, value  # type: ignore
                        )

                        # We can't tell what stage of execution an in-progress
                        # workflow was in, and we don't want to suddenly launch
                        # a whole bunch at once, so we just terminate them.
                        if not is_status_terminal(entity.status):
                            entity.status = EntityStatus.terminated
                            entity.reason = EntityStatusReason.interrupted
                            value = entity.json().encode("utf-8")

                        if not self._dryrun:
                            self._pending.add(input_item.message_id)
                            producer.produce(
                                # Backups must be restored to the *events* topic so that
                                # workflows-aggregator can update its internal state
                                config.kafka.topics.workflows_events,
                                value=value,
                                key=key,
                                callback=self._on_produce_acknowledged,
                            )
                        else:
                            self._out_queue.put(
                                _OutputItem(
                                    worker_id=self.id,
                                    message_id=input_item.message_id,
                                    success=True,
                                    message="dryrun",
                                )
                            )

                    except Exception as ex:
                        self._out_queue.put(
                            _OutputItem(
                                worker_id=self.id,
                                message_id=input_item.message_id,
                                success=False,
                                message=" ".join(ex.args),
                            )
                        )

                if not self._dryrun:
                    producer.poll()

            except queue.Empty:
                time.sleep(0.1)

        # Wait for acknowledgements
        if not self._dryrun:
            producer.flush()
        # sentinel
        self._out_queue.put(None)
        if self._pending:
            raise RuntimeError(
                f"worker {self.id} terminated with {len(self._pending)} pending messages"
            )


def _require_entity_id(s: str) -> str | None:
    try:
        p = pathlib.Path(s)
        id = uuid.UUID(hex=p.name)
        if id.version == 4:
            return id.hex
    except:
        pass
    return None


@click.group()
def backup():
    pass


@backup.command()
@click.option(
    "confirm",
    "--confirm",
    metavar="CONFIRM",
    is_flag=True,
    default=False,
    help="Execute the restore operation",
)
@click.option(
    "num_workers",
    "--num-workers",
    metavar="NUM_WORKERS",
    type=int,
    default=1,
    help="Number of worker processes to use for producing messages",
)
@click.option(
    "suppress_warnings",
    "--suppress_warnings",
    metavar="SUPPRESS_WARNINGS",
    is_flag=True,
    default=False,
    help="Don't print warning messages",
)
@click.option(
    "dryrun",
    "--dryrun",
    metavar="DRYRUN",
    is_flag=True,
    default=False,
    help="Simulate restore process without making state changes",
)
@click.option(
    "chunk_size",
    "--chunk-size",
    metavar="CHUNK_SIZE",
    type=int,
    default=128,
    help="Send inputs in 'chunks' to reduce IPC overhead",
)
@click.option(
    "verbose",
    "--verbose",
    metavar="VERBOSE",
    is_flag=True,
    default=False,
    help="Log extra information to stdout",
)
def restore(
    confirm: bool,
    num_workers: int,
    suppress_warnings: bool,
    dryrun: bool,
    chunk_size: int,
    verbose: bool,
):
    if config.backup.storage_path is None:
        raise click.ClickException("DYFF_BACKUP__STORAGE_PATH not set")

    click.echo(
        f"plan: restore from {config.backup.storage_path} to {config.kafka.topics.workflows_events}"
    )

    def inputs():
        object_paths = storage.list_dir(config.backup.storage_path, recursive=False)

        for p in object_paths:
            id = _require_entity_id(p)
            if id is not None:
                yield _InputItem(message_id=id, object_path=p)
            elif not suppress_warnings:
                click.echo(
                    f"WARNING: skipping path that does not look like an entity message: {p}"
                )

    # TODO: Loading the whole list into memory might be a problem some day
    input_list = list(inputs())
    click.echo(f"plan: {len(input_list)} objects will be restored")

    if confirm:
        # Execute the backup

        in_queue: mp.Queue[list[_InputItem] | None] = mp.Queue(maxsize=1024)
        out_queue: mp.Queue[_OutputItem | None] = mp.Queue()

        success_count: int = 0
        failures: list[_OutputItem] = []

        click.echo(f"apply: replaying messages using {num_workers} workers")

        def outputs() -> Iterable[_OutputItem]:
            num_finished = 0
            while num_finished < num_workers:
                output = out_queue.get()
                if output is None:
                    num_finished += 1
                else:
                    yield output

        workers = [
            _Worker(id, in_queue, out_queue, dryrun=dryrun) for id in range(num_workers)
        ]

        for worker in workers:
            worker.start()

        def _fill_input_queue():
            chunk = []
            for input_item in input_list:
                chunk.append(input_item)
                if len(chunk) >= chunk_size:
                    in_queue.put(chunk)
                    chunk = []
            # Last chunk (partial)
            if chunk:
                in_queue.put(chunk)
                chunk = []
            # sentinel
            for _ in range(num_workers):
                in_queue.put(None)

        input_thread = threading.Thread(target=_fill_input_queue)
        input_thread.start()

        with click.progressbar(outputs(), length=len(input_list)) as progress:
            for output_item in progress:
                if verbose:
                    click.echo(f"verbose: processed {output_item}")
                if output_item.success:
                    success_count += 1
                else:
                    failures.append(output_item)

        for failure in failures:
            click.echo(f"ERROR: message {failure.message_id}: {failure.message}")
        click.echo(f"apply: replayed {success_count} messages")

        for worker in workers:
            worker.join()

        if failures:
            raise RuntimeError("restore incomplete")
