# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

import os
from typing import List, Optional

from pydantic import BaseModel, BaseSettings, Field, SecretStr

from dyff.storage.config import StorageConfig


class KafkaConfigConfig(BaseModel):
    bootstrap_servers: str = Field(
        default="kafka.kafka.svc.cluster.local",
        description="The address to contact when establishing a connection to Kafka.",
        examples=[
            "kafka.kafka.svc.cluster.local",
            "kafka.kafka.svc.cluster.local:9093",
        ],
    )

    client_id: Optional[str] = Field(default=None)
    compression_type: str = "zstd"
    enable_idempotence: bool = True

    def get_producer_config(self, keys: Optional[List[str]] = None):
        if keys is None:
            keys = [
                "bootstrap.servers",
                "client.id",
                "compression.type",
                "enable.idempotence",
            ]
        return {key: self.get_by_kafka_key(key) for key in keys}

    def get_by_kafka_key(self, key: str):
        field_name = key.replace(".", "_")
        return getattr(self, field_name)


class KafkaTopicsConfig(BaseModel):
    workflows_events: str = Field(
        default="dyff.workflows.events",
        examples=["test.workflows.events"],
    )
    workflows_state: str = Field(
        default="dyff.workflows.state",
        examples=["test.workflows.state"],
    )


class KafkaConfig(BaseModel):
    config: KafkaConfigConfig = Field(default_factory=KafkaConfigConfig)
    topics: KafkaTopicsConfig = Field(default_factory=KafkaTopicsConfig)


class BackupConfig(BaseModel):
    api_endpoint: Optional[str] = Field(description="Dyff API endpoint", default=None)
    api_token: SecretStr = Field(
        description="Dyff API token with read permission for all resources",
        default=None,
    )

    storage_path: Optional[str] = Field(
        description="Storage path in s3, e.g. 's3://some/path'", default=None
    )


class DyffConfig(BaseSettings):
    kafka: KafkaConfig = Field(default_factory=KafkaConfig)
    storage: StorageConfig = Field(default_factory=StorageConfig)

    backup: BackupConfig = Field(default_factory=BackupConfig)

    class Config:
        env_file = os.environ.get("DYFF_DOTENV_FILE", ".env")
        # Env variables start with 'DYFF_'
        env_prefix = "dyff_"
        # Env var like 'DYFF_KAFKA__FOO' will be stored in 'kafka.foo'
        env_nested_delimiter = "__"


config = DyffConfig()
